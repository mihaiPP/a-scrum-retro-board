const STORAGE_KEY = 'scrum-tasks'
export default {
  fetch: function () {
    const tasks = JSON.parse(localStorage.getItem(STORAGE_KEY) || '{"backlog": [], "worksForUs": [], "improvements": [], "suggestions":[]}')

    return tasks
  },
  save: function (tasks) {
    localStorage.setItem(STORAGE_KEY, JSON.stringify(tasks))
  }
}
