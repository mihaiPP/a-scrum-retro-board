# a-scrum-retro-board

<img src="./src/assets/tenor.gif" width="100"/>

## [Live Demo](https://gracious-bartik-377df5.netlify.app/)

## App preview

<img src="./src/assets/a-scrum-retro-board.gif" width="600"/>

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

# Requirements

### A Scrum Board (similar to Miro Board) to work with widgets (stickies).

## Details

Operations to be be provided by the web service:

- [x] Create a sticky which must have AS MINIMUM:
  - [x] Color
  - [x] Text
  - [x] You are free to add other attributes
  - [x] The newer the sticky the higher it lies on the plane Stickies must be able to be moved anywhere on the board
- [x] Stickies can be removed from the board ONLY manually, otherwise stickies should remain on the board after refreshing the page or closing the browser.
- [x] All basic CRUD operations are mandatory
- [x] You are free to choose the tools and libraries you will be using
- [x] At least 30% of the code should be covered by tests
- [x] Create a good documentation on how the project can be started and used
- [x] Submit sources via a public Git repository

# Implementation

## Tools used

- main framework [vueJS](https://vuejs.org/)
- ui-library [vue-material](https://vuematerial.io/)
- to persist the data used browser local-storage
- drag and drop functionality covered by [vue-draggable](https://github.com/SortableJS/Vue.Draggable)
- application was deployed to [Netlify](https://www.netlify.com/)

## Possible improvements

- use [vuex](https://vuex.vuejs.org/guide/#the-simplest-store) for application state
- create a theme for vue-material (instead of abusing bad practices `!important`)
- add up/down votes for each task
