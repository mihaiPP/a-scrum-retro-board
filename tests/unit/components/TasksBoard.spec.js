import { shallowMount } from '@vue/test-utils'
import TasksBoard from '@/components/TasksBoard.vue'

describe('TasksBoard.vue', () => {
  it('should be a vue instance', () => {
    const wrapper = shallowMount(TasksBoard, {
      propsData: {
        allTasks: {
          backlog: [],
          worksForUs: [],
          improvements: [],
          suggestions: []
        }
      }
    })
    expect(wrapper.vm).toBeTruthy()
  })
})
