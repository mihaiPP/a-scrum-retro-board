import { mount } from '@vue/test-utils'
import AddEditItem from '@/components/AddEditItem.vue'

describe('AddEditItem.vue', () => {
  it('should be a vue instance', () => {
    const wrapper = mount(AddEditItem)
    expect(wrapper.vm).toBeTruthy()
  })

  it('should emit on-close when close button is clicked', async () => {
    const wrapper = mount(AddEditItem)

    const buttons = wrapper.findAllComponents({ name: 'md-button' })

    await buttons.at(1).trigger('click')
    expect(wrapper.emitted()['on-close']).toBeTruthy()
  })

  it('should emit on-save when save button is clicked', async () => {
    const wrapper = mount(AddEditItem)

    const buttons = wrapper.findAllComponents({ name: 'md-button' })
    const input = wrapper.findComponent({ name: 'md-input' })
    await input.setValue('New title')
    // TODO : find how to set the select value [vue-test-utils]: wrapper.setValue() cannot be called on this element
    // const select = wrapper.findComponent({ name: 'md-select' })
    // const options = select.findAll('md-option')
    // options.at(1).setSelected()
    // const select = wrapper.findComponent({ name: 'md-select' })
    // const option = wrapper
    //   .findComponent({ name: 'md-select' })
    //   .findAllComponents({ name: 'md-option' })
    // option.at(1).element.selected = true

    // // select.element.value = 'blue'
    // await select.trigger('change')

    await buttons.at(0).trigger('click')
    expect(wrapper.emitted()['on-save']).toBeTruthy()
    expect(wrapper.emitted()['on-save'][0]).toEqual([
      { title: 'New title', id: undefined, color: undefined }
    ])
  })

  it('should emit on-save when save button is clicked and props passed', async () => {
    const wrapper = mount(AddEditItem, {
      propsData: {
        title: 'Old title',
        id: 123,
        color: 'blue'
      }
    })

    const buttons = wrapper.findAllComponents({ name: 'md-button' })
    const input = wrapper.findComponent({ name: 'md-input' })
    await input.setValue('New title')
    await buttons.at(0).trigger('click')
    
    expect(wrapper.emitted()['on-save']).toBeTruthy()
    expect(wrapper.emitted()['on-save'][0]).toEqual([
      { title: 'New title', id: 123, color: 'blue' }
    ])
  })
})
