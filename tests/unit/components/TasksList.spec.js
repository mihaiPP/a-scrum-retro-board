import { shallowMount, mount } from '@vue/test-utils'
import TasksList from '@/components/TasksList.vue'

describe('TasksList.vue', () => {
  it('should be a vue instance', () => {
    const wrapper = shallowMount(TasksList, {
      propsData: {
        tasks: [],
        title: ''
      }
    })
    expect(wrapper.vm).toBeTruthy()
  })
  it('should render list title correctly', () => {
    const wrapper = shallowMount(TasksList, {
      propsData: {
        tasks: [],
        title: 'title text'
      }
    })
    expect(wrapper.text()).toContain('title text')
  })

  it('should render task list correctly', () => {
    const wrapper = shallowMount(TasksList, {
      propsData: {
        tasks: [
          { id: 123, title: 'task1', color: 'blue' },
          { id: 321, title: 'task2', color: 'red' }
        ],
        title: 'title text'
      }
    })
    expect(wrapper.text()).toContain('title text')
    expect(wrapper.findAllComponents({ name: 'TaskItem' })).toHaveLength(2)
  })

  it('should show the AddEditItem component', async () => {
    const wrapper = mount(TasksList)
    const buttons = wrapper.findComponent({ name: 'md-button' })
    await buttons.trigger('click')
    expect(wrapper.vm.isAddTask).toBeTruthy()
    expect(wrapper.findComponent({ name: 'AddEditItem' }).exists()).toBe(true)
  })
})
