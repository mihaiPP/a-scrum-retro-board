import { shallowMount, mount } from '@vue/test-utils'
import TaskItem from '@/components/TaskItem.vue'

describe('TaskItem.vue', () => {
  it('should be a vue instance', () => {
    const wrapper = shallowMount(TaskItem, {
      propsData: {
        title: 'test',
        color: 'red',
        id: '123'
      }
    })
    expect(wrapper.vm).toBeTruthy()
  })

  it('should be render correctly the item', () => {
    const wrapper = shallowMount(TaskItem, {
      propsData: {
        title: 'test',
        color: 'red',
        id: '123'
      }
    })
    expect(wrapper.text()).toContain('test')
    expect(wrapper.classes()).toContain('red')
    expect(wrapper.findComponent({ name: 'AddEditItem' }).exists()).toBe(false)
  })

  it('should handle delete item', async () => {
    const wrapper = mount(TaskItem, {
      propsData: {
        title: 'test',
        color: 'red',
        id: '123'
      }
    })
    const buttons = wrapper.findAllComponents({ name: 'md-button' })
    await buttons.at(1).trigger('click')
    expect(wrapper.findComponent({ name: 'AddEditItem' }).exists()).toBe(false)
    expect(wrapper.emitted()['on-delete']).toBeTruthy()
  })

  it('should handle add item', async () => {
    const wrapper = mount(TaskItem, {
      propsData: {
        title: 'test',
        color: 'red',
        id: '123'
      }
    })
    const buttons = wrapper.findAllComponents({ name: 'md-button' })
    await buttons.at(0).trigger('click')
    expect(wrapper.vm.isEditing).toBeTruthy()
    expect(wrapper.findComponent({ name: 'AddEditItem' }).exists()).toBe(true)
  })
})
